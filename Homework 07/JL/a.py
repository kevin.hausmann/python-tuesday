import csv
from typing import Optional

DATABASE_FILENAME = 'database.csv'


def delete_time_series_items(identifier: str) -> Optional[dict]:
    """
    Read and delete all entries in a time series with given ID from database.
    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.
    :param str identifier: Time series ID as stored in the database.
    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    series = None
    with open(DATABASE_FILENAME, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            if identifier == line['ID']:
                series = {}
                for key, value in line.items():
                    if value is not None:
                        # We want the year keys to be ints not strs, so we need to convert those
                        series[int(key) if key.isdigit() else key] = value
                for value in line.items():
                    value = None  # delete values in line
                    deletewriter = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                    if identifier == line['ID']:
                        deletewriter.writerow({series})  # write back empty cells

    return series


def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
read_time_series(dummy)
