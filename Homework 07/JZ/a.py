""" 
    f1: get_database_reader 
    f2: is_time_series_present 
    f3: write new data into database and save 
    f4: remove all time series 
    f5: read time series 
"""
import csv

DATABASE_FILENAME = 'database.csv'
DELIMITER = ','
QUOTE_CHAR = '"'
identifier = 'ID'
FIELDNAMES = ["ID","Name","Wertetyp","Region","Sektor","Brennstoff","Gas","Einheit",
                "1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000",
              "2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011",
              "2012","2013","2014","2015","2016","2017","2018","2019","2020"]

def _get_database_reader():
    """ 
    To open the database file in read mode and get reader object 
    """
    with open(DATABASE_FILENAME, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR)
    return reader


def is_time_series_present(identifier: str) -> bool:
    """ 
    To check if a time series with the given ID exist or not 
    :param identifier: given ID for the to be searched time series 
    :return:  if ID exists, return True otherwise remains False 
    """
    result = False
    with open(DATABASE_FILENAME, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR)
        # if reader = _get_database_reader() is used here: ValueError: I/O operation on closed file.
        for row in reader:
            if row[0] == identifier:
                result = True
                break
    return result


def save_time_series(identifier: str, data: dict) -> str:
    """ 
    This method saves a new time series with given identifier and data. 
    :param identifier: given new ID for the to be saved time series 
    :param data: given data in a dictionary containing keys and values as new time series 
    :return: if ID exists, return relevant information, if not, write the new data into the database and save, 
    return relevant information 
    """
    result = None
    with open (DATABASE_FILENAME, 'a', newline='') as out_file:
        writer = csv.DictWriter(out_file, fieldnames=FIELDNAMES)
    if is_time_series_present(identifier) is False:
        writer.writerows(data)
        result = print('Data has been successully saved!')
    else:
        result = print('Time series ID already exists and data cannot be saved!')

    return result


def remove_all_time_series() -> None:
    """ 
    This function is to delete all time series in the csv file and remains only the keys 
    """
    with open(DATABASE_FILENAME,'w', newline='') as out_file:
        writer = csv.DictWriter(out_file, fieldnames=FIELDNAMES)
        writer.writeheader()
    return None


def read_time_series(identifier: str):
    """ 
    Read and return time series with given ID from database. 
    This method searches the database for any time series with the given identifier. If found, 
    the corresponding series is returned, including all its data. If not found, None is returned. 
    :param str identifier: Time series ID as stored in the database. 
    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints, 
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted. 
    """
    series = None
    with open(DATABASE_FILENAME, 'r') as c_file:
        reader = csv.reader(c_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR)
        for line_counter, content in enumerate(reader):
            if line_counter == 0:
                keys = content
            elif identifier == content[0]:
                series = {}
                for key,value in zip(keys,content):
                    if value is not None:
                        key = int(keys) if keys.isdigit() else keys
                        # key = int(key) if key.isdigit() else key? key or keys
                        series[key] = value
                break
    return series

def init_database() -> bool:
    pass

def init_database_with_dummy_data(number_of_time_series: int) -> bool:
    pass

######################## Test code below this line ##############################

def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]

    other_example_series = read_time_series('#52044111')
    assert '#52044111' == other_example_series[ID_COLUMN_KEY]
    assert 'Example AR third' == other_example_series['Name']
    assert 'Diesel' == other_example_series['Brennstoff']
    assert 'TJ' == other_example_series['Einheit']
    assert 2019.0 == other_example_series[2019]
    assert 2020.0 == other_example_series[2020]


def test_is_time_series_present():
    assert False is is_time_series_present(None)
    assert False is is_time_series_present('XXXX--key that cannot possibly exist!--XXXX')
    assert False is is_time_series_present('')

def test_save_time_series():
    assert False is save_time_series(None)
