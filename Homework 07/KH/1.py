import csv
import fileinput
from typing import Optional, Set

DATABASE_FILENAME = 'database.csv'
DELIMITER = ','
QUOTE_CHAR = '"'

CSV_HEADER = '"ID","Name","Wertetyp","Region","Sektor","Brennstoff","Gas","Einheit",' \
             '"1990","1991","1992","1993","1994","1995","1996","1997","1998","1999",' \
             '"2000","2001","2002","2003","2004","2005","2006","2007","2008","2009",' \
             '"2010","2011","2012","2013","2014","2015","2016","2017","2018","2019",' \
             '"2020"'
FIELD_NAMES = CSV_HEADER[1:-1].split('","')
ID_COLUMN_KEY = 'ID'


#################### Python Tuesday API #########################################


def init_database() -> bool:
    pass


def init_database_with_test_data() -> None:
    pass


def init_database_with_dummy_data(number_of_time_series: int) -> bool:
    pass


def is_time_series_present(identifier: str) -> bool:
    pass


def read_time_series(identifier: str) -> Optional[dict]:
    """
    Read and return time series with given ID from database.

    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.

    :param str identifier: Time series ID as stored in the database.

    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    series = None
    with open(DATABASE_FILENAME, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            if identifier == line[ID_COLUMN_KEY]:
                series = {}
                for key, value in line.items():
                    if value is not None and len(str(value)) > 0:
                        # We want the year keys to be ints not strs, so we need to convert those
                        series[int(key) if key.isdigit() else key] = value
                # At this point, the requested time series was found and retrieved from the database.
                # Since IDs are unique, we stop our search here and return the values read.
                break

    return series


def read_time_series_matching_filter(key: str, value: str) -> Set[dict]:
    pass


def save_time_series(identifier: str, data: dict) -> bool:
    """
    Persist time series in database. 

    This method stores a time in the database. If the ID given already exists, the existing series
    will be updated. If the ID is not yet present the time series will be added to the database.

    :param str identifier: Time series ID to search for.
    :param dict data: Time series information to store. You can give a value of 'None' here to delete an existing entry.

    :return: True if the a new database record has been created, i.e. the time series did not previously exist. False otherwise.
    """
    all_data: dict = {}
    new_series_created = False
    
    # 1. Read all data into memory
    with open(DATABASE_FILENAME, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            all_data = {**all_data, line[ID_COLUMN_KEY]: line}
    
    # 2. Write back all data to the same file, making the requested changes as we go
    with open(DATABASE_FILENAME, 'w', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=FIELD_NAMES,
                                delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        writer.writeheader()
        
        # 2.1. Add time series, if not present
        if identifier not in all_data.keys() and identifier is not None and len(identifier) > 0:
            all_data[identifier] = {ID_COLUMN_KEY: identifier}
            new_series_created = True
        
        # 2.2 Write data back to file, updating time series as needed
        for series in all_data.values(): 
            # Check if we found the time series that should change, if yes: change it!
            if identifier == series[ID_COLUMN_KEY]:
                for key, value in data.items():
                    if str(key) in FIELD_NAMES:
                        series[str(key)] = value
            
            writer.writerow(series)
    
    # 3. Clean out empty fields in the csv
    with fileinput.FileInput(DATABASE_FILENAME, inplace=True) as csv_file:
        for line in csv_file:
            print(line.replace(',""', ','), end='')
    
    return new_series_created


def remove_all_time_series() -> None:
    pass


######################## Test code below this line ##############################

def test_read_time_series():
    init_database_with_test_data()
    
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]

    other_example_series = read_time_series('#52044111')
    assert '#52044111' == other_example_series[ID_COLUMN_KEY]
    assert 'Example AR third' == other_example_series['Name']
    assert 'Diesel' == other_example_series['Brennstoff']
    assert 'TJ' == other_example_series['Einheit']
    assert 2019.0 == other_example_series[2019]
    assert 2020.0 == other_example_series[2020]


def test_save_time_series_data():
    init_database_with_test_data()
    
    assert not save_time_series(None, None)
    assert not save_time_series(None, dict())
    assert not save_time_series('', dict())
    assert save_time_series('XXXX--key that cannot possibly exist!--XXXX', dict())
    
    updated_data = {'Name': 'Test', 'Einheit': 'km', 2000: 42}
    assert not save_time_series('#52001122', updated_data)
    read_back = read_time_series('#52001122')
    assert 'Test' == read_back['Name']
    assert 42 == read_back[2000]
    
    assert save_time_series('#OtherNewID', updated_data)
    read_back = read_time_series('#OtherNewID')
    assert 'Test' == read_back['Name']
    assert 42 == read_back[2000]
    
    partly_bogus_data = {None: 'Zero', 'Fox': 'Bla', 'Name': 'Can you believe?',
                         1990: 42., 2000: None, 2050: 42.}
    assert not save_time_series('#52001122', partly_bogus_data)
    read_back = read_time_series('#52001122')
    assert 'Fox' not in read_back.keys()
    assert 'Can you believe?' == read_back['Name']
    assert 42. == read_back[1990]
    assert 2000 not in read_back.keys()
    assert 2050 not in read_back.keys()
    

test_read_time_series()
test_save_time_series_data()
