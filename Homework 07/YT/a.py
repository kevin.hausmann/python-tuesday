import csv

import pandas

DATABASE_FILE = 'database.csv'

def read_time_series(identifier: str) -> optional[dicht]:
    
    """
    Read and return time series with given ID from database.
    
    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.
    :param str identifier: Time series ID as stored in the database.
    
    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """
    series = None
    with open(DATABASE_FILE, newline='') as file:
        reader = csv.reader(file, delimiter= ",")
        for row in enumerate(reader):
            if identifier == 0:
                series = row
        for key, value in row.items():
                if value is not None:
                     if key.isdigit() == True:
                            key = int(key)
                            series[key] = value
                else:
                    series[key] = value
    return (series)

def filter_series(key_input: str, value_input: str):

    with open(DATABASE_FILE, newline='') as file:
        reader = csv.reader(data)
        id_set: set = set({})
        for row in reader:
            for key, value in row.items():
                if key == key_input and value == value_input:
                    id_set.add(row['ID'])

    return id_set


def test_filter_series():
    assert filter_series(None, None) == set()

    assert filter_series('Wertetyp', 'AR') == {'#52001331', '#52044111', '#52001111'}
    assert filter_series('Gas', 'CO2') == {'#52001122'}
    assert filter_series('Brennstoff', 'Diesel') == {'#52001122', '#52001331', '#52044111'}
    assert len(filter_series('Wertetyp', 'AR')) == 3
    assert len(filter_series('Sektor', 'Railways')) == 4

    example_set = filter_series('Einheit', 'TJ')
    assert type(example_set) == set
    for i in example_set:
        assert type(i) == str

test_filter_series()
