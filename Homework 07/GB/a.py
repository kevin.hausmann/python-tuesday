import csv

DATABASE_FILE = 'database.csv'
DELIMITER = ','
QUOTECHAR = '"'

def read_time_series(identifier: str):
    '''
       function to read time serie by giving ID defined as string from a csv
       output is a dictionary with pair of keys and value
    '''

    new_row = None
    with open(DATABASE_FILE, newline='') as data:
        data_reader = csv.DictReader(data, delimiter=DELIMITER, quotechar=QUOTECHAR, quoting=csv.QUOTE_NONNUMERIC)
        for row in data_reader:
            if identifier == row['ID']:
                new_row = {}
                for key, value in row.items():
                    if value is not None:
                        if key.isdigit() == True:
                            key = int(key)
                            new_row[key] = value
                        else:
                            new_row[key] = value
    return (new_row)

def filter_series(key_input: str, value_input: str):
    '''
       function to filter timeseries by definition the value of a key from time series in csv
       output is a set of time series ids for reading with the funtion read_time_series()
    '''

    with open(DATABASE_FILE, newline='') as data:
        data_reader = csv.DictReader(data, delimiter=DELIMITER, quotechar=QUOTECHAR, quoting=csv.QUOTE_NONNUMERIC)
        id_set: set = set({})
        for row in data_reader:
            for key, value in row.items():
                if key == key_input and value == value_input:
                    id_set.add(row['ID'])

    return id_set


def test_filter_series():
    assert filter_series(None, None) == set()

    assert filter_series('Wertetyp', 'AR') == {'#52001331', '#52044111', '#52001111'}
    assert filter_series('Gas', 'CO2') == {'#52001122'}
    assert filter_series('Brennstoff', 'Diesel') == {'#52001122', '#52001331', '#52044111'}
    assert len(filter_series('Wertetyp', 'AR')) == 3
    assert len(filter_series('Sektor', 'Railways')) == 4

    example_set = filter_series('Einheit', 'TJ')
    assert type(example_set) == set
    for i in example_set:
        assert type(i) == str


test_filter_series()

### Verknüpfung von filter_series und read_time_series zur Ausgabe der gefilterten Zeitreihen

set_time_series: list = []

for i in filter_series('Brennstoff', 'Diesel'):
    set_time_series += [read_time_series(i)]

print(set_time_series)
