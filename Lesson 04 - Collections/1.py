# Container properties: order, mutable, duplicates (there are more...)

# list: ordered, mutable, can have duplicates
secrets = [True, 32561232, 3.14, 651328421, "651328421", True]

# tuple: ordered, immutable, can have duplicates
#secrets = (True, 32561232, 3.14, 651328421, "651328421", True)

# set: not ordered, mutable, no duplicates
#secrets = {True, 32561232, 3.14, 651328421, "651328421", True}

# dict: ordered(?), mutable, no duplicate keys
#secrets = {'a': True, 'a': 32561232, 'b': 3.14, 'c': 651328421, 'd': "651328421", 'e': True}

# Simple for loop
for value in secrets:
    print(f'Value {value} has type {type(value)}')

# Use while instead
index = 0
while index < len(secrets):
    print(f'Value {secrets[index]} at index {index} has type {type(secrets[index])}')
    index += 1

# For loop with index
for index, value in enumerate(secrets):
    print(f'Value {value} at index {index} has type {type(value)}')

# Change container contents
secrets.append(False)
secrets[1] = 42
secrets = ['First'] + secrets

# See results
for index, value in enumerate(secrets):
    print(f'Value {value} at index {index} has type {type(value)}')
