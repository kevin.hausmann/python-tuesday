from python_tuesday_api import  read_time_series, save_time_series

# Task: MyEmissions = (#52001331 + #52044111) * #52001122

START_YEAR = 1990
END_YEAR = 2020

example_activity_data = read_time_series('#52001331')
other_activity_data = read_time_series('#52044111')
matching_emission_factor = read_time_series('#52001122')

my_emissions = {'Name': 'My emission result',
                'Wertetyp': 'EM',
                'Sektor': 'Railways',
                'Brennstoff': 'Diesel',
                'Gas': 'CO2',
                'Einheit': 'kg'}

for year in range(START_YEAR, END_YEAR + 1):
    my_emissions[year] = (example_activity_data[year] + other_activity_data[year]) * matching_emission_factor[year]

save_time_series('#MyEmissions', my_emissions)
