from python_tuesday_api import save_time_series, read_time_series

start_year: int = 1990
end_year: int = 2020


diesel_ef: dict = read_time_series('#52001122')
diesel_ar_de: dict = read_time_series('#52044111')
diesel_ar_abl: dict = read_time_series('#52001331')
diesel_em: dict = {"Name":'Emission',"Wertetyp":'EM',"Region":'EU',"Sektor":'Railways',"Brennstoff":'Diesel',"Gas":'CO2',"Einheit":'kg'}

for year in range(start_year, end_year+1,1):
    diesel_em[year] = (diesel_ar_de[year]+diesel_ar_abl[year])*diesel_ef[year]

save_time_series ('#52001351', diesel_em)
