import csv

DATABASE_FILE = 'database.csv'
DELIMITER = ','
QUOTECHAR = '"'


def read_time_series(identifier: str):
    '''
       function to read time serie by giving ID defined as string from a csv
       output is a dictionary with pair of keys and value
    '''

    new_row = None
    with open(DATABASE_FILE, newline='') as data:
        data_reader = csv.DictReader(data, delimiter=DELIMITER, quotechar=QUOTECHAR, quoting=csv.QUOTE_NONNUMERIC)
        for row in data_reader:
            if identifier == row['ID']:
                new_row = {}
                for key, value in row.items():
                    if value is not None:
                        if key.isdigit() == True:
                            key = int(key)
                            new_row[key] = value
                        else:
                            new_row[key] = value
    return (new_row)


def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
