import csv

def read_time_series (file):
    database = []
    with open(file, newline='') as data:
        data_reader = csv.DictReader(data, delimiter=',')
        for row in data_reader:
            new_row = {}
            for key in row:
                if row[key] is not None:
                    new_row[key] = row[key]
            database.append(new_row) 
            
    return (database)

read_time_series('database.csv')
