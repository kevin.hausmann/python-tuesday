import csv

def read_time_series (file):
    database = []
    with open(file, newline='') as data:
        data_reader = csv.DictReader(data, delimiter=',')
        for row in data_reader:
            new_row = {}
            for key in row:
                if row[key] is not None:
                    new_row[key] = row[key]
            database.append(new_row) 

        for dictionary in database:
            if len(dictionary) >7:
                print(dictionary) # bei diesem dict dann die Keys in :int und die Werte in :float umwandeln und in Liste ersetzen
                        
    return (database)

read_time_series('database.csv')
