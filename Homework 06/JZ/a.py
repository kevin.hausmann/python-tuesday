import csv

DATABASE_FILENAME = 'database.csv'

def read_time_series(identifier: str) -> dict | None:
    """
    Read and return time series with given ID from database.
    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.
    :param str identifier: Time series ID as stored in the database.
    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    with open('database.csv','r') as c_file:   
        reader = csv.Reader(c_file,delimiter=',')
        line_counter = 0
        for row in reader:
            if line_counter == 0:
                keys = row
                line_counter += 1
                continue
            if row[0] == identifier:
                dict_keys = []
                dict_values = []
                for i in range(len(row)) and row[i] != None:
                    if i < 8:
                        dict_keys.append(keys[i])
                        dict_values.append(row[i])
                    else:
                        dict_keys.append(int(keys[i]))
                        dict_values.append(float(row[i]))
                time_series_dict = dict(zip(dict_keys,dict_values))
                return time_series_dict
            else:
                line_counter += 1
    return None

def test_read_time_series():
    assert None == read_time_series(None)
    assert None == read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
