import csv

DATABASE_FILENAME = 'database.csv'


def read_time_series(identifier: str):
    """
    Read and return time series with given ID from database.
    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.
    :param str identifier: Time series ID as stored in the database.
    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    result = None
    with open('database.csv', 'r') as c_file:
        reader = csv.reader(c_file, delimiter=',')
        for line_counter, row in enumerate(reader):
            if line_counter == 0:
                keys = row
            elif row[0] == identifier:
                dict_keys = []
                dict_values = []
                for index, value in enumerate(row):
                    if value is not None:
                        dict_keys.append(keys[index] if index < 8 else int(keys[index]))
                        dict_values.append(value if index < 8 else float(value))
                result = dict(zip(dict_keys, dict_values))

    return result


def test_read_time_series():
    assert None == read_time_series(None)
    assert None == read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
