import csv

# Change basedir to your path of preference!
BASEDIR = '/media/christian/flash/python_tuesday_280322/Python-Tuesday/Homework 06/CM'
DATABASE_FILENAME = f'{BASEDIR}/database.csv'
NEWLINE = '\n'
QUOTE = '"'
DELIMITER = ','


def read_time_series(identifier: str) -> dict:
    """
    Function for reading data from a csv time series line by line
    :param filepath: path to your .csv
    :param identifier: the identifier of your time series
    :returns: Series if found, or None
    """
    series = None
    if type(identifier) == str:
        with open(DATABASE_FILENAME, newline=NEWLINE) as file:
            reader = csv.reader(file, delimiter=DELIMITER, quotechar=QUOTE)
            for index, row in enumerate(reader):
                if index == 0:
                    header = list(row)
                    for indexh, item in enumerate(header):
                        if item.isdigit():
                            header[indexh] = float(item)
                if identifier in row:
                    for indexa, itema in enumerate(row):
                        try:
                            row[indexa] = float(itema)
                        except ValueError:
                            row[indexa] = itema
                    series = dict(zip(header, row))

    return series


def test_read_time_series():
    assert None == read_time_series(None)
    assert None == read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
