from typing import Optional

import pandas

DATABASE_FILENAME = 'database.csv'


def read_time_series(identifier: str) -> Optional[dict]:
    """
    Read and return time series with given ID from database.

    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.

    :param str identifier: Time series ID as stored in the database.

    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    series = None
    database = pandas.read_csv(DATABASE_FILENAME, index_col=0)
    if identifier in database.index:
        temp = {'ID': identifier} | database.loc[identifier].dropna().to_dict()  # The | syntax needs Python > 3.10
        # Convert year keys from strs to ints
        series = {(int(key) if key.isdigit() else key): value for key, value in temp.items()}

    return series


def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
