from typing import Optional

DATABASE_FILENAME = 'database.csv'


def read_time_series(identifier: str) -> Optional[dict]:
    """
    Read and return time series with given ID from database.

    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.

    :param str identifier: Time series ID as stored in the database.

    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    series = None
    with open(DATABASE_FILENAME, mode='rt') as csv_file:
        for index, line in enumerate(csv_file):
            # Use line content minus its ending (e.g. \r\n), split by comma (,) and remove quotes (")
            line = line.rstrip()
            values = []
            for value in line.split(','):
                if value[0] == '"':
                    values += [value[1:-1]]
                else:
                    values += [value]

            if index == 0:
                keys = values
            elif identifier == values[0]:
                series = {}
                for key, value in zip(keys, values):
                    if value is not None:
                        # We want the year keys to be ints and their values to be floats (not strs)
                        key, value = (int(key), float(value)) if key.isdigit() else (key, value)
                        series[key] = value
                break

    return series


def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
