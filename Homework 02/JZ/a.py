import random

print('Hallo welcome to the game! Now please input a number and the computer will guess what it is')
your_number = int(input('Please input a number:'))
try_this=[0,100]
all_guessed = []
i=0
Computer = random.randint(0,100)  
while i < 10:
    if your_number == Computer:
        print(f'Bingo! The computer got it in {i} times, and the correct number is {your_number}')
        all_guessed.append(Computer)
        break
    elif your_number > Computer:
        print('The number should be larger,try again')
        try_this.remove(min(try_this))
        try_this.append(Computer)
        all_guessed.append(Computer)
        Computer = random.randint(min(try_this), max(try_this))   #Computer might repeat the same number as long as it fits this
        i += 1
        continue
    else:
        print('The number should be smaller,try again')
        try_this.remove(max(try_this))
        try_this.append(Computer)
        all_guessed.append(Computer)
        Computer = random.randint(min(try_this), max(try_this))
        i += 1
        continue
print(f'the computer guessed numbers are {all_guessed}')
