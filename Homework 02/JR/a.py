import sys
import random

print("Sie müssen sich eine Zahl zwischen 1 und 100 aussuchen und der Computer versucht sie zu erraten.")
myguess = int(input("Geben Sie ihre Zahl ein: "))
print (f"Sie haben sich die Zahl {myguess} ausgesucht.")

print("Jetzt rät der Computer")

randomnumber = -1
count_of_guess = 0
list_of_numbers = []
lower_frontier = 0
higher_frontier =100


while myguess != randomnumber:
     randomnumber =(random.randint(lower_frontier, higher_frontier))
     list_of_numbers += [randomnumber]
     count_of_guess += 1
     print(f"Der Computer hat die Zahl {randomnumber} gewählt")

     if myguess > randomnumber:
         print("Die zu ratende Zahl ist größer!")
         lower_frontier = randomnumber+1
     elif myguess < randomnumber:
         print("Die zu ratende Zahl ist kleiner!")
         higher_frontier = randomnumber-1

print(f"Der Computer hat die Zahl mit {count_of_guess} Versuchen erraten")
print(f"Der Computer hat dazu folgenden Zahlen gewählt {list_of_numbers}")
