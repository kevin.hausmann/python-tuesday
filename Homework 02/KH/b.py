min_limit, max_limit = 50, 250
guess, attempt_count = -1, 0

print(f"Welcome! Please think of a number from {min_limit} to {max_limit} and do not tell me!")
input("Hit Enter when you are ready!")

user_input: str = ''
while user_input != 'r':
    guess, attempt_count = int((min_limit + max_limit) / 2), attempt_count + 1
    print(f"My {attempt_count}. guess is that your number is: {guess}")

    user_input = input("Am I (r)ight? Is your number (h)igher or (l)ower? Please type 'r', 'h', or 'l' and hit Enter!")
    min_limit = guess if user_input == 'h' else min_limit
    max_limit = guess if user_input == 'l' else max_limit

print(f"That's it! Your number was {guess}.")
