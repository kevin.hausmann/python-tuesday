maxint=100
minint=1
tipp= 0
help = ''

print ("Denke Dir bitte eine Zahl, ich versuche deine Zahl zu raten, bitte hilf mir und gib '<' wenn die Zahl kleiner als deine Zahl ist, '>'wenn die Zahl größer als deine Zahl ist und '=' ein, wenn die Zahl stimmt")

while help != '=':
    tipp = int ((minint + maxint) /2)
    print(f"Mein Tipp ist:{tipp}")
    help = input("Hilf mir bitte, <, >, = ?")
    if help == '>':
        minint = tipp
    elif help == '<':
        maxint = tipp

print("Hurra! Gewonnen, die Zahl ist richtig!!!")
