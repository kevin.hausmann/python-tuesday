from random import randint

x = int(0)
y = int(100)
a = int(0)

print(
    f'Denke dir eine Zahl zwischen {x} und {y} aus, ich werde sie erraten! Sag mir, ob deine Zahl größer mit ">" oder kleiner mit "<" als meine geratene Zahl ist!')

while True:

    r = randint(x, y)
    print(f'Ist deine Zahl {r}?')
    i = str(input('Ist deine Zahl >, <, = ?:'))

    if i == '=':
        print(f' Juhu. Ich habe deine Zahl erraten! Sie ist also {r}. Nur {a} Versuche dafür sind doch ziemlich gut.')
        break
    if i == '>':
        x = r + 1
        a = a + 1
        print('Ok, neuer Versuch, ich komme drauf!')
    if i == '<':
        y = r - 1
        a = a + 1
        print('Ok, neuer Versuch, ich komme drauf!')
