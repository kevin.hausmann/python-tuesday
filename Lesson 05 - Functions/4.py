# 1. Define function header
# 2. Add documentation of intended use
# 3. Put test cases
# 4. Implement

def calculate_char_frequency(sentence: str, letter: str) -> tuple[float, int, bool]:
    """
    Examine given string and calculate some properties, including the proportional representation
    of the given character. All calculations ignore case.

    Return a tuple(frequency in percent, length of the sentence, occurrence of the letter at least once).
    """
    sentence, letter = sentence.upper(), letter.upper()

    letter_count = 0
    for char in sentence:
        if char == letter:
            letter_count += 1

    frequency = .0
    if len(sentence) > 0:
        frequency = letter_count / len(sentence)

    occurrence = False
    if len(letter) > 0:
        occurrence = letter in sentence

    return frequency * 100, len(sentence), occurrence


def test_calculate_char_frequency():
    assert (.0, 0, False) == calculate_char_frequency('', '')
    assert (100., 1, True) == calculate_char_frequency('a', 'A')
    assert (100., 2, True) == calculate_char_frequency('aA', 'A')
    assert (.0, 2, False) == calculate_char_frequency('aA', 'b')
    assert (.0, 2, False) == calculate_char_frequency('aA', 'B')

    assert (3 / 15 * 100, 15, True) == calculate_char_frequency('Kevin is stupid', 'i')
    assert (1 / 15 * 100, 15, True) == calculate_char_frequency('Kevin is stupid', 'p')


test_calculate_char_frequency()
