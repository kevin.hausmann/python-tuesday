# 1. Define function header
# 2. Add documentation of intended use
# 3. Put test cases
# 4. Implement

def calculate_char_frequency(sentence: str, letter: str) -> float:
    """
    Examine given string and calculate proportional representation
    of the given character. All calculations ignore case.
    Only single letters are allowed.

    Returns letter frequency as percentage.
    Example usage ('Lazy fox', 'x') == 1/8
    """
    sentence = str(sentence).lower() if sentence is not None else ''
    letter = str(letter).lower() if letter is not None else ''

    count = sum([1 for char in sentence if char == letter])
    return count / len(sentence) * 100 if len(sentence) > 0 else 0.


def test_calculate_char_frequency():
    assert 0. == calculate_char_frequency('', '')
    assert 0. == calculate_char_frequency('lazy fox', '')
    assert 0. == calculate_char_frequency('', None)
    assert 0. == calculate_char_frequency(None, None)
    assert 0. == calculate_char_frequency(None, '')
    assert 0. == calculate_char_frequency('lazy fox', 'fox')

    assert 1 / 8 * 100 == calculate_char_frequency('lazy fox', 'x')
    assert 1 / 8 * 100 == calculate_char_frequency('LAZY FOX', 'x')
    assert 1 / 8 * 100 == calculate_char_frequency('lazy fox', 'X')
    assert 0. == calculate_char_frequency('lazy fox', '0')
    assert 100 == calculate_char_frequency('a', 'a')
    assert 50 == calculate_char_frequency('ab', 'a')
    assert 1 / 3 * 100 == calculate_char_frequency('abc', 'c')

    assert 2 / 10 * 100 == calculate_char_frequency('#lazy #fox', '#')
    assert 2 / 10 * 100 == calculate_char_frequency('!lazy !fox', '!')
    assert 1 / 8 * 100 == calculate_char_frequency('lazy fox', ' ')

    assert 1 / 10 * 100 == calculate_char_frequency('0123456789', '6')
    assert 1 / 10 * 100 == calculate_char_frequency('0123456789', 6)
    assert 1 / 9 * 100 == calculate_char_frequency(123456789, 6)


test_calculate_char_frequency()
