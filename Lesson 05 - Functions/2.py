# 1. Define function header
# 2. Add documentation of intended use
# 3. Put test cases
# 4. Implement

def create_hint(secret: str, guess: str, expected_length: int = 5, indicators: tuple = ('x', 'o', 'i')) -> str:
    """
    Generate user feedback for given guess and secret Wordle.

    Uses 'x' for 'character is not in Wordle', 'o' for 'character is in Wordle,
    but at a different place' and 'i' for 'character guessed matches secret'.
    Override these choices by providing your own tuple of indicators.

    Will always return a string of len() == 5, composed of those three characters.
    """
    secret, guess = secret.upper(), guess.upper()
    no, meh, yes = indicators
    hint = no * expected_length

    if len(guess) == expected_length:
        hint = ''
        for index, char in enumerate(guess):
            if char in secret:
                # Chars may occur more than once, so a simple secret.index() is not enough here!
                char_at_indices = [idx for idx, letter in enumerate(secret) if letter == char]
                hint += yes if index in char_at_indices else meh
            else:
                hint += no

    return hint


def test_create_hint():
    assert 'xxxxx' == create_hint('Kante', 'Primo')
    assert 'iiiii' == create_hint('kante', guess='KANTE')
    assert 'iixii' == create_hint(secret='Kante', guess='Karte')
    assert 'ooxxx' == create_hint(secret='Kante', guess='Anzug')

    #assert 'xxxxx' == create_hint('Kante', None)
    assert 'xxxxx' == create_hint('Kante', '')
    assert 'xxxxx' == create_hint('Kante', '12345')
    assert 'xxxxx' == create_hint('Kante', 'kurz')
    assert 'xxxxx' == create_hint('Kante', 'viel zu lang')

    assert 'aaaaa' == create_hint('Kante', 'Primo', indicators=('a', 'b', 'c'))
    assert 'ccacc' == create_hint(secret='Kante', guess='Karte', indicators=('a', 'b', 'c'))
    assert 'bbaaa' == create_hint(secret='Kante', guess='Anzug', indicators=('a', 'b', 'c'))


test_create_hint()
