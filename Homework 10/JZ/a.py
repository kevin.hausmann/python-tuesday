"""Task Description:
Create a program that calculates the number of days since a person's birthday.
As part of this, write a function that gets two dates as input (parameters)
and returns the number of days. Make sure to properly document and test your function!
Define function to calculate number of days between two dates'''
"""

import datetime as dt
import math


def calculate_days(date1, date2):
    """
    given two different dates, calculate how many days between these two dates
    :param date1:someones birthday
    :param date2:some chosen date
    :return:the number of days between the two given dates
    """
    date1 = dt.datetime.strptime(date1, "%Y-%m-%d")
    date2 = dt.datetime.strptime(date2, "%Y-%m-%d")
    num_of_days = (date2 - date1).days
    return math.fabs(num_of_days)


# print(calculate_days('2010-08-07','1988-01-05'))

def check_is_dates(dates):
    try:
        dt.datetime.strptime(dates, "%Y-%m-%d")
        return True
    except:
        return False


if __name__ == '__main__':
    birthday = input('please input the birthday in the format yyyy-mm-dd:')
    someday = input('please input the date in the format yyyy-mm-dd:')
    if check_is_dates(birthday) and check_is_dates(someday):
        num = calculate_days(birthday, someday)
        print(f'there are {num} days between the two given dates')
    else:
        print('please input the right format for the dates and try again')
