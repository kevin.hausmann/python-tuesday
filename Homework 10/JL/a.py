# 1. Define function header
# 2. Add documentation of intended use
# 3. Put test cases
# 4. Implement

# Create a program that calculates the number of days since a person's birthday.
# As part of this, write a function that gets two dates as input (parameters) and returns
# the number of days. Make sure to properly document and test your function!

# ---------------
# Define function to calculate number of days between two dates

# Ask user for their birth-year
# Ask user for their birth-month
# Ask user for their birth-day

# Use function to get result
# Display result
from datetime import date


def date_diff(firstday: date, lastday: date) -> int:
    """
    Calculates the difference between two given dates
    :param lastday: last day of the calculation
    :param firstday: first day of the calculation
    :type datetime: object
    :return diff_days: the count of the days or 0 if negative values
    or strings as firstday or lastday is given
    """
    diff_days: timedelta = lastday - firstday  # if isinstance(lastday, firstday (int, float)) else 0
    return diff_days.days


def test_date_diff():
    assert 0 == firstday(0), lastday(0)
    assert 0 == firstday(1), lastday(1)
    assert 16 == firstday(1), lastday(17)
    assert 0 == firstday(-1), lastday(-17)
    assert 0 == type.firstday(string), type.lastday(string)
    assert 0 == firstday > datetime.today()


# Input birthdate
day = input('Gib Deinen Geburtstag im Format "T" ein: ')
month = input('Gib Deinen Geburtsmonat im Format "M" ein: ')
year = input('Gib Dein Geburtsjahr im Format "JJJJ" ein: ')

# cast string to datetime
birthday: date = date(int(year), int(month), int(day))

# usertest
print('Dein Geburtstag ist also:')
print(birthday.strftime('%A, %B %d, %Y'))

# allocate todays date to variable
lastday = date.today()

# Call function date_diff
diff_days: int = date_diff(birthday, lastday)

#output
print(f'You already live for {diff_days} days!')
