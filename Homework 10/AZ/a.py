# Create a program that calculates the number of days since a person's birthday.
# As part of this, write a function that gets two dates as input (parameters) and returns
# the number of days. Make sure to properly document and test your function!

# ---------------
# Define function to calculate number of days between two dates

# Ask user for their birth-year
# Ask user for their birth-month
# Ask user for their birth-day

# Use function to get result
# Display result

from datetime import datetime, timedelta


# Define function to calculate number of days between two dates
def calculate_days(start_datetime: datetime, end_datetime: datetime) -> int:
    # Calculate timedelta
    delta: timedelta = end_datetime - start_datetime

    # Return the days of the delta (could also return e.g. delta.seconds)
    return delta.days


# Ask user for their birth-year
year: str = input('Your birth year (e.g. 2000): ')

# Ask user for their birth-month
month: str = input('Your birth month (e.g. 01): ')

# Ask user for their birth-day
day: str = input('Your birth day (e.g. 01): ')

# Combine strings to complete date
date: str = f'{day}.{month}.{year}'

# Convert date string to datetime object
date_format: str = '%d.%m.%Y'
start: datetime = datetime.strptime(date, date_format)

# Get current timestamp as datetime object
end: datetime = datetime.now()

# Call defined function
days: int = calculate_days(start, end)

# Output the result
print(f'From {start} to {end} {days} have passed.')