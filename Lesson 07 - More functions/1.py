import csv
from typing import Optional, Set

DATABASE_FILENAME = 'database.csv'
DELIMITER = ','
QUOTE_CHAR = '"'

ID_COLUMN_KEY = 'ID'


def init_database() -> bool:
    pass


def init_database_with_dummy_data(number_of_time_series: int) -> bool:
    pass


def is_time_series_present(identifier: str) -> bool:
    pass


def read_time_series(identifier: str) -> Optional[dict]:
    """
    Read and return time series with given ID from database.

    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.

    :param str identifier: Time series ID as stored in the database.

    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    series = None
    with open(DATABASE_FILENAME, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            if identifier == line[ID_COLUMN_KEY]:
                series = {}
                for key, value in line.items():
                    if value is not None:
                        # We want the year keys to be ints not strs, so we need to convert those
                        series[int(key) if key.isdigit() else key] = value
                # At this point, the requested time series was found and retrieved from the database.
                # Since IDs are unique, we stop our search here and return the values read.
                break

    return series


def read_time_series_matching_filter(key: str, value: str) -> Set[dict]:
    pass


def save_time_series(identifier: str, data: dict) -> bool:
    pass


def remove_all_time_series() -> None:
    pass


######################## Test code below this line ##############################

def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]

    other_example_series = read_time_series('#52044111')
    assert '#52044111' == other_example_series[ID_COLUMN_KEY]
    assert 'Example AR third' == other_example_series['Name']
    assert 'Diesel' == other_example_series['Brennstoff']
    assert 'TJ' == other_example_series['Einheit']
    assert 2019.0 == other_example_series[2019]
    assert 2020.0 == other_example_series[2020]


test_read_time_series()
