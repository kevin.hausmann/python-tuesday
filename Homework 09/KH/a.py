from python_tuesday_api import *

# Task: Implement AEB

START_YEAR = 1990
END_YEAR = 2020

init_database_with_dummy_data(42)

# Find all activity data time series in database
for ad_series in read_time_series_matching_filter('Wertetyp', 'AR'):
    print(f'Activity data found: {ad_series}')

    # Find matching emission factors
    for ef_series in read_time_series_matching_filter('Wertetyp', 'EF'):
        if ad_series['Region'] == ef_series['Region'] and \
                ad_series['Sektor'] == ef_series['Sektor'] and \
                ad_series.get('Brennstoff', None) == ef_series.get('Brennstoff', None):
            print(f'Matching emission factor found: {ef_series}')

            # Create resulting emission time series
            em_series = ef_series
            em_series['Name'] = 'AEB-Ergebnis'
            em_series['Wertetyp'] = 'EM'
            em_series['Einheit'] = 't'
            # Calculate emissions
            for year in range(START_YEAR, END_YEAR + 1):
                em_series[year] = ad_series[year] * ef_series[year]

            print(f'Emissions calculated: {em_series}')
            # Create unique identifier for resulting time series
            while is_time_series_present(em_series['ID']):
                em_series['ID'] = f'#52AEB{random.randint(100, 999)}'

            save_time_series(em_series['ID'], em_series)
