"""Test module for the Python Tuesday API"""

import os

from python_tuesday_api import *


def _init_database_with_test_data() -> None:
    init_database()
    save_time_series('empty', dict())
    save_time_series('dummy', {'Name': 'Dummy'})

    more_test_data: list = [
        '"example","Example","EX","EXA","Sect","ExFuel","Poll","EX",1990.5,1991.5,1992.5,1993.5,1994.5,1995.5,1996.5,1997.5,1998.5,1999.5,2000.5,2001.5,2002.5,2003.5,2004.5,2005.5,2006.5,2007.5,2008.5,2009.5,2010.5,2011.5,2012.5,2013.5,2014.5,2015.5,2016.5,2017.5,2018.5,2019.5,2020.5',
        '"#52001111","Example AR","AR","DE","Railways","","","TJ",1990.42,1991.42,1992.42,1993.42,1994.42,1995.42,1996.42,1997.42,1998.42,1999.42,2000.42,2001.42,2002.42,2003.42,2004.42,2005.42,2006.42,2007.42,2008.42,2009.42,2010.42,2011.42,2012.42,2013.42,2014.42,2015.42,2016.42,2017.42,2018.42,2019.42,2020.42',
        '"#52001122","Example EF","EF","DE","Railways","Diesel","CO2","kg/TJ",1990.23,1991.23,1992.23,1993.23,1994.23,1995.23,1996.23,1997.23,1998.23,1999.23,2000.23,2001.23,2002.23,2003.23,2004.23,2005.23,2006.23,2007.23,2008.23,2009.23,2010.23,2011.23,2012.23,2013.23,2014.23,2015.23,2016.23,2017.23,2018.23,2019.23,2020.23',
        '"#52001331","Example AR other","AR","ABL","Railways","Diesel","","TJ",1990.99,1991.99,1992.99,1993.99,1994.99,1995.99,1996.99,1997.99,1998.99,1999.99,2000.99,2001.99,2002.99,2003.99,2004.99,2005.99,2006.99,2007.99,2008.99,2009.99,2010.99,2011.99,2012.99,2013.99,2014.99,2015.99,2016.99,2017.99,2018.99,2019.99,2020.99',
        '"#52044111","Example AR third","AR","DE","Railways","Diesel","","TJ",1990.0,1991.0,1992.0,1993.0,1994.0,1995.0,1996.0,1997.0,1998.0,1999.0,2000.0,2001.0,2002.0,2003.0,2004.0,2005.0,2006.0,2007.0,2008.0,2009.0,2010.0,2011.0,2012.0,2013.0,2014.0,2015.0,2016.0,2017.0,2018.0,2019.0,2020.0'
    ]

    with open(DATABASE_FILENAME, mode='a', encoding=ENCODING, newline='\n') as csv_file:
        csv_file.writelines(f'{data_set}\n' for data_set in more_test_data)


def test_init_database():
    if os.path.isfile(DATABASE_FILENAME):
        os.remove(DATABASE_FILENAME)

    init_database()
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING, newline='\n') as file:
        assert CSV_HEADER == file.readline()


def test_init_database_with_dummy_data():
    pass


test_init_database_with_dummy_data()


def test_is_timeseries_present():
    pass


test_is_timeseries_present()


def test_read_time_series():
    assert None is read_time_series(None)
    assert None is read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]

    other_example_series = read_time_series('#52044111')
    assert '#52044111' == other_example_series[ID_COLUMN_KEY]
    assert 'Example AR third' == other_example_series['Name']
    assert 'Diesel' == other_example_series['Brennstoff']
    assert 'TJ' == other_example_series['Einheit']
    assert 2019.0 == other_example_series[2019]
    assert 2020.0 == other_example_series[2020]


def test_read_time_series_matching_filter():
    pass


def test_save_time_series_data():
    assert not save_time_series(None, None)
    assert not save_time_series(None, dict())
    assert not save_time_series('', dict())
    assert save_time_series('XXXX--key that cannot possibly exist!--XXXX', dict())

    updated_data = {'Name': 'Test', 'Einheit': 'km', 2000: 42, 2010: 'NE'}
    assert not save_time_series('#52001122', updated_data)
    read_back = read_time_series('#52001122')
    assert 'Test' == read_back['Name']
    assert 42 == read_back[2000]
    assert 'NE' == read_back[2010]

    assert save_time_series('#OtherNewID', updated_data)
    read_back = read_time_series('#OtherNewID')
    assert 'Test' == read_back['Name']
    assert 42 == read_back[2000]

    partly_bogus_data = {None: 'Zero', 'Fox': 'Bla', 'ID': 'bogus', 'Name': 'Can you believe?',
                         1990: 42., 2000: None, 2050: 42.}
    assert not save_time_series('#52001122', partly_bogus_data)
    read_back = read_time_series('#52001122')
    assert 'Fox' not in read_back.keys()
    assert '#52001122' == read_back['ID']
    assert 'Can you believe?' == read_back['Name']
    assert 42. == read_back[1990]
    assert 2050 not in read_back.keys()


def test_remove_timeseries():
    pass


test_remove_timeseries()

if __name__ == '__main__':
    print('Running test suite for the Python Tuesday API...')
    test_init_database()

    _init_database_with_test_data()
    test_read_time_series()

    _init_database_with_test_data()
    test_save_time_series_data()
    print('Success, no errors.')
