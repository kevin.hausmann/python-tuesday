"""
Simple API to simulate a Mesap/TechStack-style time series database and allow for
calculation scripts as offered by the Seven2one Platform's CalQlator module.
"""

import csv
from typing import Optional, Set
from csv import DictWriter

DATABASE_FILENAME = 'database.csv'
ENCODING = 'UTF-8'
DELIMITER = ','
QUOTE_CHAR = '"'

ID_COLUMN_KEY = 'ID'
CSV_HEADER = f'"{ID_COLUMN_KEY}","Name","Wertetyp","Region","Sektor","Brennstoff","Gas","Einheit",' \
             '"1990","1991","1992","1993","1994","1995","1996","1997","1998","1999",' \
             '"2000","2001","2002","2003","2004","2005","2006","2007","2008","2009",' \
             '"2010","2011","2012","2013","2014","2015","2016","2017","2018","2019",' \
             '"2020"'
FIELD_NAMES = CSV_HEADER[6:-1].split('","')  # Also remove "ID" at the beginning

NAME = 'Example'
WERTETYP = 'EX'
REGION = 'EXA'


def init_database() -> None:
    """Make sure the database file exists and contains the csv header. Delete any existing data."""
    with open(DATABASE_FILENAME, mode='w+', encoding=ENCODING, newline='\n') as database_file:
        database_file.writelines(CSV_HEADER)


def init_database_with_dummy_data(number_of_time_series: int) -> bool:
    '''
    The function fills the database with sample data:
    A dictionary with dummy data is created
    Then this dummy data is written into the database by a DictWriter
    '''
    headersCSV = ['ID', 'Name', 'Wertetyp', 'Region']
    dummy_dict = {'ID': 'dummy_id', 'Name': 'dummy_name', 'Wertetyp': 'dummy_wertetyp', 'Region': 'dummy_region'}

    with open(DATABASE_FILENAME, 'w+', newline='') as csv_writer:
        dictwriter_object = DictWriter(csv_writer, fieldnames=headersCSV)
        dictwriter_object.writerow(dummy_dict)


init_database_with_dummy_data(2)


def is_time_series_present(identifier: str) -> bool:
    '''
    checks if timeseries with the given ID exists in the database
    : param str identifier: ID to check for the timeseries
    : return: True if the ID exists
    '''
    with open(DATABASE_FILENAME, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            if row["ID"] == identifier:
                print("Yes, the time series is present!")
                return True
    return False


def read_time_series(identifier: str) -> Optional[dict]:
    """
    Read and return time series with given ID from database.
    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.
    :param str identifier: Time series ID as stored in the database.
    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    series = None
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            if identifier == line[ID_COLUMN_KEY]:
                series = {}
                for key, value in line.items():
                    if value is not None and len(str(value)) > 0:
                        # We want the year keys to be ints not strs, so we need to convert those
                        series[int(key) if key.isdigit() else key] = value
                # At this point, the requested time series was found and retrieved from the database.
                # Since IDs are unique, we stop our search here and return the values read.
                break

    return series


def read_time_series_matching_filter(key: str, value: str) -> Set[dict]:
    '''
    This function compares with the given filter options whether the filter options are available in the database.
    the filter options are compared with the values from the database:
    the keys and values which should be compared with the database are stored in a dictionary
    finally the corresponding ID is returned from the database in a dictionary
    '''
    dict_filter = {'DB_Name': NAME,
                   'Wertetyp': WERTETYP,
                   'Region': REGION
                   }
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            for key, value in line.items():
                newdict = {}
                if dict_filter.values in line.items():
                    newdict[str(key)] = line[key]
                    newdict[str(value)] = line['ID']
    return (newdict)


read_time_series_matching_filter(NAME, 'Example')


def save_time_series(identifier: str, data: dict) -> bool:
    """
    Persist time series in database.
    This method stores a time series in the database. If the ID given already exists, the existing series
    will be updated. If the ID is not yet present, a new time series is added to the database.
    :param str identifier: Time series ID to search for.
    :param dict data: Time series information to store. IDs given as part of the data are ignored.
    You can give a value of 'None' here with any dimension or year to delete an existing entry.
    :return: True iff the a new database record has been created, i.e. the time series did not previously exist.
    """
    all_data: dict = {}
    new_series_created = False

    # 1. Read all data into memory
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            all_data = {**all_data, line[ID_COLUMN_KEY]: line}

    # 2. Write back all data to the same file, making the requested changes as we go
    with open(DATABASE_FILENAME, mode='w', encoding=ENCODING, newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=[ID_COLUMN_KEY] + FIELD_NAMES,
                                delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        writer.writeheader()

        # 2.1. Add time series, if not present
        if identifier not in all_data.keys() and identifier is not None and len(identifier) > 0:
            all_data[identifier] = {ID_COLUMN_KEY: identifier}
            new_series_created = True

        # 2.2 Write data back to file, updating time series as needed
        for series in all_data.values():
            # Check if we found the time series that should change, if yes: change it!
            if identifier == series[ID_COLUMN_KEY]:
                for key, value in data.items():
                    if str(key) in FIELD_NAMES:
                        series[str(key)] = value

            writer.writerow(series)

    return new_series_created


def remove_all_time_series() -> None:
    """
    This function is to delete all time series in the csv file
    only the keys remains
    """
    with open(DATABASE_FILENAME, mode='w+', encoding=ENCODING, newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=field_names,
                                delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        writer.writeheader()


if __name__ == '__main__':
    init_database_with_dummy_data(100)
