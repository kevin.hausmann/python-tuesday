import csv
import random as rd

def init_database_with_dummy_data (number_of_time_series):
    
    list_time_series: list = []
    database_with_dummy_data_created=False
    
    for i in range(0, number_of_time_series):
        
        id_list: list = ['#',]
        id_character_count: int = 8
        for id_index_number in range(0, id_character_count):
            id_list_number = rd.randint(0,id_character_count)
            id_list += [id_list_number]
        id_ = "".join([str(number) for number in id_list])
  
        start_year: int = 1990
        stop_year: int = 2020

        time_series: dict = {}
        
        #it's possible to add new elements as the descriptor of the dimension
        key: list = ['ID', 'Name', 'Wertetyp', 'Region', 'Sektor', 'Brennstoff', 'Gas', 'Einheit']
        name: list = ['']
        region: list =['DE', 'ABL', 'EU', 'IRL', 'GB', 'FIN', 'FR', 'BE', 'NL']
        sector: list =['Schienenverkehr', 'Straßenverkehr', 'Flugverkehr', 'Private Haushalte', 'Landwirtschaft', 'Militär', 'Forstwirtschaft', 'GHD', 'Industrie']
        energy_source:list = ['Steinkohle','Braunkohle', 'Erdgas', 'Diesel', 'Flüssiggas', 'Heizöl-leicht', 'Heizöl-schwer', 'Ottokraftstoff', 'Flüssige biogene Stoffe', 'Feste biogene Stoffe', 'Biogas']
        
        #every new value-type, needs a new unit condition
        value_type: list = ['AR', 'EM', 'EF', 'EX'] 
        unit_ar: list = ['TJ']
        unit_em: list = ['CO2', 'CH4']
        unit_ef: list = ['kg/TJ']
        unit_ex: list = ['EX']

        index_name = rd.randint(0, len(name)-1)
        name_value = name[index_name]

        index_region = rd.randint(0, len(region)-1)
        region_value = region[index_region]

        index_sector = rd.randint(0, len(sector)-1)
        sector_value = sector[index_sector] 

        index_energy_source = rd.randint(0, len(energy_source)-1)
        energy_source_value = energy_source[index_energy_source] 

        index_value_type = rd.randint(0,len(value_type)-1)
        value_type_value = value_type[index_value_type] 
        if value_type_value == 'AR':
            index_unit_ar = rd.randint(0, len(unit_ar)-1)
            unit_value = unit_ar[index_unit_ar] 
        if value_type_value == 'EM':
            index_unit_em = rd.randint(0, len(unit_em)-1)
            unit_value = unit_em[index_unit_em] 
        if value_type_value == 'EF':
            index_unit_ef = rd.randint(0, len(unit_ef)-1)
            unit_value = unit_ef[index_unit_ef] 
        if value_type_value == 'EX':
            index_unit_ex = rd.randint(0, len(unit_ex)-1)
            unit_value = unit_ex[index_unit_ex] 

        time_series_list = [id_, name_value, value_type_value, region_value, sector_value, energy_source_value, unit_value]

        time_series: dict = dict(zip(key, time_series_list))

        # smaller random range for random value of time serie over years
        start_random_range = rd.randint (0, 1000000)
        end_random_range = rd.randint (start_random_range, 1000000)

        for year in range (start_year, stop_year+1, 1):
            if time_series ['Wertetyp'] == 'AR':
                time_series[year] = rd.randint (start_random_range, end_random_range)
            elif time_series ['Wertetyp'] == 'EM':
                time_series[year] = None
            else:
                time_series[year] = (rd.uniform (start_random_range, end_random_range))/10000
       
        #check: new id is already in list of time series dictionary 
        write_in_list:bool = True
        for dict_id in list_time_series:
            if dict_id['ID'] == time_series['ID']:
                write_in_list = False
            elif len(time_series['ID'])!=9:
                write_in_list = False
        if write_in_list == True:
            list_time_series += [time_series]
       
    if len(list_time_series) == number_of_time_series: 
        header:list = key
        for year in range (start_year, stop_year+1, 1):
            header += [year]
        
        with open('database.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=header)
            writer.writeheader()
            for dict_time_series in list_time_series:
                writer.writerow(dict_time_series)
            
        database_with_dummy_data_created=True
    else:
        database_with_dummy_data_created=False
    
    return database_with_dummy_data_created
