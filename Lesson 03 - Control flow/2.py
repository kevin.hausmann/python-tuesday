worldviews = [False, True, True, True, False, True] # bool
tokens = ['siefrebf', 'gsgshshs', 'ASRR162'] # str
payments = [.99, 3.19, 42., 6.66, 89.99, -142] # 
mixed = [42, False, 3.14, 'Tadaa!'] # int, bool, float, str

total: int = 0
print(type(total))
for amount in payments:
    total += amount

print(f"Sum of {payments} is: {total}")
print(type(total))
