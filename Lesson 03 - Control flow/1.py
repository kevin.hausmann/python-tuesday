numbers = [3, 5, 1, 10, -13, 42]

print(f"I have a list of numbers with {len(numbers)} entries:")
for number in numbers:
    print(number)
