"""This is Homework 3

Create two time series: activity data (AD) and emission factor (EF)
Select start and stop years (e.g. 1990 to 2020)
Fill time series with random values for all years
AD values should be 100 +- random 20
EF values should be 0.5 +- random .3
Calculate (EMyear = ADyear * EFyear) and store emissions in new time series (EM)
Print yearly and total results

"""

import random

import numpy


class Ha3_numpy:
    def __init__(self, start: int, stop: int, adfactor: int = 100, effactor: float = 0.5):
        """
        Numpy Solution
        :param start: First year of your timeseries
        :param stop: Last year of your timeseries
        :param adfactor: Activity Data number HW (100)
        :param effactor: EMission Factor number HW (0.5)
        """
        self.startdate = start
        self.stopdate = stop
        self.years = numpy.arange(self.startdate, self.stopdate + 1)
        self.ts = numpy.ones_like(self.years)
        self.ad = self.ts * adfactor + numpy.random.randint(-20, 20, size=self.years.shape[0])
        self.ef = self.ts * effactor + numpy.random.uniform(-0.3, 0.3, [self.years.shape[0]])
        self.emyr = self.ef * self.ad
        a = numpy.dstack((self.years, self.emyr))
        print(f"This is the whole timeseries: {a}")
        print(f"This is the overall total {numpy.sum(self.emyr)}")


class HA3_lists:
    def __init__(self, start: int, stop: int, adfactor: int = 100, effactor: float = 0.5):
        """
        List Solution
        :param start: First year of your timeseries
        :param stop: Last year of your timeseries
        :param adfactor: Activity Data number HW (100)
        :param effactor: EMission Factor number HW (0.5)
        """
        self.startdate = start
        self.stopdate = stop + 1
        self.years = [j for j in range(self.startdate, self.stopdate)]
        self.ef = [effactor + random.uniform(-0.3, 0.3) for j in range(len(self.years))]
        self.ad = [adfactor + random.uniform(-20, 20) for j in range(len(self.years))]
        self.summ = 0
        self.emyr = []
        for j in enumerate(self.ef):
            self.summ += (self.ef[j[0]] * self.ad[j[0]])
            self.emyr.append(j[1] * self.ad[j[0]])
        self.emlistus = list(zip(self.years, self.emyr))
        print(f"This is the whole timeseries: {self.emlistus}")
        print(f"This is the overall total {self.summ}")
