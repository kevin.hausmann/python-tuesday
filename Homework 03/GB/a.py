import random as rd

ad: list = []
ef: list = []
time: list = []
em1: list = []

ts: int = 1990
te: int = 2020

while ts <= te:
    a: int = rd.randint(20, 100)
    e: float = rd.uniform(0.3, 0.5)

    time += [ts]
    ad += [a]
    ef += [e]

    # Multiplikation der Listenelemente bevor sie in die Liste Em1 geschrieben werden
    emission1 = a * e
    em1 += [emission1]

    ts += 1


# Funktion zur Berechnung der Summe über eine Liste
def total(x):
    total = .0
    for y in x:
        total += y
    return (total)


em1_total: float = total(em1)
ad_total: int = total(ad)
ef_total: float = total(ef)

print(f'Anzahl der Listeneinträge in: AD: {len(ad)}, EF: {len(ef)}, Time: {len(time)}, EM1: {len(em1)}')
print('')
print(
    f'Die Summe über alle Jahre({te - 30}-{te}) beträgt für die Listen: Emissionen: {round(em1_total)}, Aktivitätsrate: {round(ad_total)}, Emissionsfaktor: {round(ef_total)}.')
print('')
print('Aufgelistet werden die jährlichen Emissionen:')
print(em1)
