import random as rd
import pandas as pd
import numpy as np
import csv
import os

ad: list = []
ef: list = []
time: list = []
em1: list = []

ts: int = 1990
te: int = 2020

while ts <= te:
    a: int = rd.randint(20, 100)
    e: float = rd.uniform(0.3, 0.5)

    time += [ts]
    ad += [a]
    ef += [e]

    ts += 1

# Multiplikation jedes Elementes einer Liste mit jedem Element einer anderen Liste nach Erstellen der beiden Listen in einer neuen Liste Em2
em2: list = np.multiply(ad, ef)


# Funktion zur Berechnung der Summe über eine Liste
def total(x):
    total = .0
    for y in x:
        total += y
    return (total)


em2_total: float = total(em2)
ad_total: int = total(ad)
ef_total: float = total(ef)

print(f' Anzahl der Listeneinträge in: AD: {len(ad)}, EF: {len(ef)}, Time: {len(time)}, EM: {len(em2)}')
print(
    f'Die Summe über alle Jahre({te - 30}-{te}) beträgt für die Liste: Emissionen: {round(em2_total)}, Aktivitätsrate: {round(ad_total)}, Emissionsfaktor: {round(ef_total)}.')
print()
print('Die jährlichen Emissionen können der Tabelle entnommen werden:')

# Schreiben der Listen in ein DataFrame
emission_df = pd.DataFrame(list(zip(time, ad, ef, em2)),
                           columns=['Jahr', 'Aktivitätsrate', 'Emissionsfaktor', 'Emissionen'])
print(emission_df)
print(f'Insgesamt wurden über den Zeitraum von {te - 30}-{te}, rund {round(em2_total)} Emissionen ausgestoßen.')
print()

# Tabelle speichern
save: str = input('Möchten Sie die Tabelle als Excel-Tabelle speichern: yes/ no:')
if save == 'yes':
    emission_df.to_excel('emission_df.xlsx')
    wd = os.getcwd()
    print(f'Du findest die Tabelle in deinem Working Directory unter: {wd}')
else:
    print('Aufwiedersehen, ihre Daten wurden wieder gelöscht.')
