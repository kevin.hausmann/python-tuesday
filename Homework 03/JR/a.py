import random

# Tell the user about the intention of the program
print(
    "Das Programm berechnet ihnen Emissionen mit Hilfe der Formel Aktivitätsrate + Emissionsfaktor über einen Zeitraum, den Sie selbst bestimmen können.\n Die Aktivitätsraten schwanken dabei zwischen 80 und 120 und die Emissionsfaktoren zwischen 0.2 und 0.8.\n")

# Create time series: activity data (AD) and emission factor (EF) and yearly emissions and overall emissions
activity_data = []
emission_factor = []
emissions_yearly = []
emissions_overall: float = 0.0

# Select start and stop years
start_year: int = int(input("Bitte geben Sie das Anfangsjahr ein: "))
end_year: int = int(input("Bitte geben Sie das Endjahr ein: "))
years: int = end_year - start_year + 1

print("Die Emissionen werden berechnet und auf 3 Dezimalstellen gerundet ausgegeben")

i = 0
while i < years:
    # Fill time series with random values for all years
    # AD values should be 100 +- random 20
    activity_data.append(random.randint(80, 120))
    # EF values should be 0.5 +- random .3
    emission_factor.append(random.uniform(0.2, .8))
    # Calculate (EMyear = ADyear * EFyear) and store emissions in new time series (EM) and print yearly results
    emissions_yearly.append(activity_data[i] * emission_factor[i])
    print(
        f"{start_year + i}      AD: {activity_data[i]}    EF: {round(emission_factor[i], 3)}       EM: {round(emissions_yearly[i], 3)}")
    # calculate total results
    emissions_overall += emissions_yearly[i]
    i += 1

# print total emissions result
print(
    f"In den {years} Jahren von {start_year} bis {end_year} wurden {round(emissions_overall, 3)} Emissionen ausgestoßen.")
