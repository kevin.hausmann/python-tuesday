# Create two time series: activity data (AD) and emission factor (EF)
# Select start and stop years (e.g. 1990 to 2020)
# Fill time series with random values for all years
  # AD values should be 100 +- random 20
  # EF values should be 0.5 +- random .3
# Calculate (EMyear = ADyear * EFyear) and store emissions in new time series (EM)
# Print yearly and total results