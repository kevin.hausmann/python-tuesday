import random
import numpy as np
from random import randint, randrange

ar_list = [80, 120]
ef_list = [0.8, 0.2]

start_year = 1990
stop_year = 2020 + 1
timeseries = stop_year - start_year

year = [i for i in range(start_year, stop_year)]
print(year)

for a in range(30):
    ar_list.append(randint(80, 120))
print(ar_list)

for a in range(30):
    ef_list.append(random.uniform(0.8, 0.2))
print(ef_list)

for a in range(30):
    em_list = np.multiply(ar_list,ef_list) #em_ = ar * ef
