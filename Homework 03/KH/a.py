import random

period = [1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000]

activity_data = []
for year in period:
    activity_data += [100 + random.randint(-20, 20)]

emission_factor = []
for year in period:
    emission_factor += [0.5 + random.uniform(-.3, .3)]

emission = []
count = 0
for year in period:
    emission += [activity_data[count] * emission_factor[count]]
    count += 1

print(activity_data)
print(emission_factor)
print(emission)
print(sum(emission))
