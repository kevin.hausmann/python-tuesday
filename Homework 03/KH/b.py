import random

period = range(1990, 2000)

activity_data = []
emission_factor = []
emission = []
for _ in period:
    ad = 100 + random.randint(-20, 20)
    ef = 0.5 + random.uniform(-.3, .3)

    activity_data += [ad]
    emission_factor += [ef]
    emission += [ad * ef]

print(activity_data)
print(emission_factor)
print(emission)
print(sum(emission))

