import random

start_year = 1990
stop_year = 2020

activity_data = []
emission_factor = []
for _ in range(start_year, stop_year + 1):
    activity_data += [100 + random.randint(-20, 20)]
    emission_factor += [0.5 + random.uniform(-.3, .3)]

emission = []
for index, _ in enumerate(activity_data):
    emission += [activity_data[index] * emission_factor[index]]

print(activity_data)
print(emission_factor)
print(emission)
print(sum(emission))
