import random

start_year = 1990
stop_year = 2020
period = range(start_year, stop_year + 1)

activity_data = [100 + random.randint(-20, 20) for _ in period]
emission_factor = [0.5 + random.uniform(-.3, .3) for _ in period]
emission = [ad * ef for ad, ef in zip(activity_data, emission_factor)]

print(activity_data)
print(emission_factor)
print(emission)
print(sum(emission))
