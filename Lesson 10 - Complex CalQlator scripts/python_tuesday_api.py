"""
Simple API to simulate a Mesap/TechStack-style time series database and allow for
calculation scripts as offered by the Seven2one Platform's CalQlator module.
"""

import csv
import random
from typing import Optional, List

DATABASE_FILENAME = 'database.csv'
ENCODING = 'UTF-8'
DELIMITER = ','
QUOTE_CHAR = '"'

ID_COLUMN_KEY = 'ID'
CSV_HEADER = f'"{ID_COLUMN_KEY}","Name","Wertetyp","Region","Sektor","Brennstoff","Gas","Einheit",' \
             '"1990","1991","1992","1993","1994","1995","1996","1997","1998","1999",' \
             '"2000","2001","2002","2003","2004","2005","2006","2007","2008","2009",' \
             '"2010","2011","2012","2013","2014","2015","2016","2017","2018","2019",' \
             '"2020"'
FIELD_NAMES = CSV_HEADER[6:-1].split('","')  # Also remove "ID" at the beginning


def init_database() -> None:
    """Make sure the database file exists and contains the csv header. Delete any existing data."""
    with open(DATABASE_FILENAME, mode='w+', encoding=ENCODING, newline='\n') as database_file:
        database_file.writelines(CSV_HEADER)


def init_database_with_dummy_data(number_of_time_series: int) -> None:
    """
    Make sure the database file exists and contains given number of time series. Delete any existing data.

    :param int number_of_time_series: Amount of time series entries, limited to max. 1000, to create in database.
    Those series' will have dummy keys and random data. When creating the series, some logic will
    be applied to make them look reasonable, e.g. activity data time series will not have a pollutant value.
    """
    init_database()

    if number_of_time_series and 0 < number_of_time_series < 1000:
        for identifier in [f'#5200{count}' for count in range(1000, 1000 + number_of_time_series)]:
            pollutant = random.choice(['CO2', 'N2O', 'CH4', 'NOx'])
            value_type: tuple = random.choice([('AR', None, 'TJ'), ('EF', pollutant, 'kg/TJ'), ('EM', pollutant, 't')])
            series = {'Name': 'Dummy sample',
                      'Wertetyp': value_type[0],
                      'Region': 'DE',
                      'Sektor': random.choice(['Railways', 'Energy', 'Waste management', 'Metal production']),
                      'Brennstoff': random.choice(['Diesel', 'Gas', 'Coal', None]),
                      'Gas': value_type[1],
                      'Einheit': value_type[2]}

            for year in [year for year in FIELD_NAMES if year.isdigit()]:
                series[year] = round(random.random() * 42, 2)

            save_time_series(identifier, series)


def is_time_series_present(identifier: str) -> bool:
    """
    Check if given ID, and consequently a time series, already exists.

    :return: True iff time series is already present in the database.
    """
    found = False
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING) as database:
        for line in database:
            if line.startswith(f'{QUOTE_CHAR}{identifier}{QUOTE_CHAR}{DELIMITER}'):
                found = True
                break

    return found


def read_time_series(identifier: str) -> Optional[dict]:
    """
    Read and return time series with given ID from database.

    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.

    :param str identifier: Time series ID as stored in the database.

    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """
    series = read_time_series_matching_filter(ID_COLUMN_KEY, identifier)
    return series[0] if len(series) > 0 else None


def read_time_series_matching_filter(key: str, value: str) -> List[dict]:
    """
    Find time series in database based on descriptor value.

    This method searches the database for any time series with the given key-value pattern.

    :param str key: Key to filter by.
    :param str value: Value for given key to filter by.

    :return: Set of time series' that match the filter. May be empty.
    """
    matching_series = []
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            if key and value and key in line.keys() and value == line[key]:
                series = {}
                for csv_key, csv_value in line.items():
                    if len(str(csv_value)) > 0:
                        # We want the year keys to be ints not strs, so we need to convert those
                        series[int(csv_key) if csv_key.isdigit() else csv_key] = csv_value

                matching_series += [series]

    return matching_series


def save_time_series(identifier: str, data: dict) -> bool:
    """
    Persist time series in database. 

    This method stores a time series in the database. If the ID given already exists, the existing series
    will be updated. If the ID is not yet present, a new time series is added to the database.

    :param str identifier: Time series ID to search for.
    :param dict data: Time series information to store. IDs given as part of the data are ignored.
    You can give a value of 'None' here with any dimension or year to delete an existing entry.

    :return: True iff the a new database record has been created, i.e. the time series did not previously exist.
    """
    all_data: dict = {}
    new_series_created = False
    
    # 1. Read all data into memory
    with open(DATABASE_FILENAME, mode='r', encoding=ENCODING, newline='') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        for line in reader:
            all_data = {**all_data, line[ID_COLUMN_KEY]: line}
    
    # 2. Write back all data to the same file, making the requested changes as we go
    with open(DATABASE_FILENAME, mode='w', encoding=ENCODING, newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=[ID_COLUMN_KEY] + FIELD_NAMES,
                                delimiter=DELIMITER, quotechar=QUOTE_CHAR, quoting=csv.QUOTE_NONNUMERIC)
        writer.writeheader()
        
        # 2.1. Add time series, if not present
        if identifier not in all_data.keys() and identifier is not None and len(identifier) > 0:
            all_data[identifier] = {ID_COLUMN_KEY: identifier}
            new_series_created = True
        
        # 2.2 Write data back to file, updating time series as needed
        for series in all_data.values(): 
            # Check if we found the time series that should change, if yes: change it!
            if identifier == series[ID_COLUMN_KEY]:
                for key, value in data.items():
                    if str(key) in FIELD_NAMES:
                        series[str(key)] = value
            
            writer.writerow(series)
    
    return new_series_created


def remove_all_time_series() -> None:
    init_database()


if __name__ == '__main__':
    init_database_with_dummy_data(100)
