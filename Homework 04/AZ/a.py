'''
Programmablauf:
- Zufallswort wird aus gegebener Liste ausgewählt
- User soll ein Wort mit 5 Buchstaben eingeben
- Fehlermöglichkeiten:
    1. Wort ist zu klein oder zu groß
    2. Groß/Kleinschreibung
    3. Leerzeichen
- Usereingabe wird mit Zufallswort abgeglichen
- wenn ein Buchstabe an der selben Stelle ist, wird dessen Position gezeigt
- wenn ein Buchstabe an einer anderen Stelle im Wort vorhanden ist, dann kommt Meldung: "Dieser Buchstabe kommt an einer anderen Position im Wort vor"
- wenn Wort nicht komplett mit allen 5 Buchstaben übereinstimmt, dann soll Programm ab User-eingabe nochmal ablaufen
- es soll bei jeder User-eingabe ein counter mitlaufen, der Usereingaben zählt
- Usereingabe soll auf 6mal begrenzt werden -> wenn beim 6.mal Zufallswort und eingegebenes Wort nicht übereinstimmen, dann Meldung""Verloren"
'''
import random

words = ['Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis', 'Regen',
         'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe', 'Hotel', 'Hütte',
         'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza', 'Sache', 'Sahne', 'Seife',
         'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy', 'Kleid', 'Leben', 'Radio', 'Regal',
         'Spiel', 'Steak', 'Stück']

for x in words:
    i = random.choice(words)
    print(i)
    break

counter = 0
liste = []

while counter < 6:
    user_input = input("Enter a word : ")
    if len(user_input) > 5:
        print("Error! Only 5 characters allowed! You put more then 5 letters!")
    if len(user_input) < 5:
        print("Error! Only 5 characters allowed! You put less than 5 letters!")
    mylist = list(user_input)
    print(mylist)
    counter += 1

    # compare which letters are the same
    set1 = set(i)
    set2 = set(mylist)
    set3 = set1.intersection(set2)
    print(set3)

    if (set(i) == set(mylist)):
        print("Lists are equal. Try again!")
    else:
        print("Lists are not equal!")

