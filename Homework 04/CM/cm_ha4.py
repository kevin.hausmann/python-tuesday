"""
Task: Programm a word guessing game
"""
import random
import re

import numpy


class Choi():
    def __init__(self):
        """
        Class that implements a word guessing game which is based on a given list
        """
        self.words = {
            'Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis', 'Regen',
            'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe', 'Hotel', 'Hütte',
            'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza', 'Sache', 'Sahne', 'Seife',
            'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy', 'Kleid', 'Leben', 'Radio', 'Regal',
            'Spiel', 'Steak', 'Stück'}
        print(f'This should be a word guessing game ! How does it work?')
        print(f'Please take a guess I\'ll give you hints')

    @staticmethod
    def patern_match2(guess,word_selection):
        pmatch = []  # Variable for pattern (letter against letter) matching
        smatch = []  # Variable for structure (letter in word) matching
        for index, char in enumerate(word_selection):
            pmatch.append(1 if guess[index] == char else 0)
            smatch.append(1 if guess[index] in word_selection else 0)
        omatch = [smatch + pmatch for smatch, pmatch in zip(smatch, pmatch)]
        return omatch

    def pattern_match(self):
        """
        Method which implements the word check based on binary coding and structured summation.
        """
        self.pmatch = [] #Variable for pattern (letter against letter) matching
        self.smatch = [] #Variable for structure (letter in word) matching
        for j in enumerate(self.__word_selection):
            char_word = j[1]
            if self.guess[j[0]] == char_word:
                self.pmatch.append(1)
            else:
                self.pmatch.append(0)
            if self.guess[j[0]] in self.__word_selection:
                self.smatch.append(1)
            else:
                self.smatch.append(0)
        self.pmatch = numpy.asarray(self.pmatch)
        self.smatch = numpy.asarray(self.smatch)
        self.omatch = self.smatch + self.pmatch

    def printmatched(self):
        """
        Method that gives hints on the player if word guessing was more or less correct
        """
        print("\n")
        print("0-Letter not in Word, 1-Letter in Word but wrong Place, 2-Letter in word correctly placed")
        print("\n")
        listchar = [str(f) for f in self.omatch]
        listword = [f for f in self.guess]
        print(listchar)
        print(listword)
        print(f'You already guessed {self.c} time(s)!')
        print("\n")
        print("\n")

    def play(self):
        """
        Method for playing the game calling the methods above successively
        """
        self.__word_selection = random.choice(tuple(self.words))
        self.__word_selection = self.__word_selection.lower()
        self.guess = "AAAA"
        print('\n')
        print('\n')
        self.c=0
        while self.guess != self.__word_selection:
            self.c+=1
            self.guess = input('Please enter your 5 character German! word:')
            self.guess = self.guess.lower()
            if bool(re.search(r'[^a-zA-ZüÜöÖäÄ]+', self.guess)):
                print("You typed in a word with a special character, try again!")
                continue
            if len(self.guess) != len(self.__word_selection):
                continue
            #self.pattern_match()
            self.omatch=Choi.patern_match2(self.guess,self.__word_selection)
            self.printmatched()
        print(f'Bingo! {self.guess} was correct!')


if __name__ == "__main__":
    game = Choi()
    game.play()
