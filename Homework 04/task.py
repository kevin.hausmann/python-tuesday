# Implement a Wordle clone!
# See https://www.powerlanguage.co.uk/wordle/

# Use the following dictionary:
words = {'Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis', 'Regen', 'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe', 'Hotel', 'Hütte', 'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza', 'Sache', 'Sahne', 'Seife', 'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy', 'Kleid', 'Leben', 'Radio', 'Regal', 'Spiel', 'Steak', 'Stück'}
