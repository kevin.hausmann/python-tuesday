import random as rd

words: list = ['Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis',
               'Regen', 'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe',
               'Hotel', 'Hütte', 'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza',
               'Sache', 'Sahne', 'Seife', 'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy',
               'Kleid', 'Leben', 'Radio', 'Regal', 'Spiel', 'Steak', 'Stück']

r: int = rd.randint(1, len(words))
rword: str = words[r]
userwords: list = []
attemps: int = 5
right: list = []
indexletter: list = []
indexnumber: list = []


def letters(x):
    numbers: int = 0
    for number in x:
        numbers += 1
    return (numbers)


while len(userwords) <= attemps:
    proof: list = []
    wordx: list = []

    iword: str = input(f'Raten sie ein Wort mit 5 Buchstaben. Sie haben noch {attemps - len(userwords) + 1} Versuche.')

    proof += [iword]
    wordx = [s for s in words if iword in s]

    if letters(iword) != 5:
        print(
            f'Das war ein Freiversuch. Ihr Wort hatte {letters(iword)}. Versuchen sie es noch einmal mit einem Wort mit genau 5 Buchstaben!')

    elif proof != wordx:
        print(f'Das war ein Freiversuch. Das Wort {iword} ist nicht in der Liste. Versuchen sie ein anderes Wort.')

    else:
        if iword == rword:
            userwords += [iword]
            print(
                f'Sie haben das Wort {rword} erraten und das Spiel nach {len(userwords)} von 6 möglichen Rateversuchen gewonnen.')
            break

        else:
            userwords += [iword]
            # print(f'Anzahl ihrer bisherigen Versuche: {len(userwords)}')
            right: list = []
            for i in rword:
                if (i in iword) == True:
                    right += [i]

            indexletter: list = []
            indexnumber: list = []
            index = 0
            while index < len(rword):
                if rword[index] == iword[index]:
                    indexletter += [iword[index]]
                    indexnumber += [index + 1]
                index += 1

    print(
        f'Bisherige Versuche: {userwords}. Im Wort enthalten sind folgende Buchstaben:{right}. Von diesen an der richtigen Stelle sind die Buchstaben {indexletter} an den Positionen :{indexnumber}.')

if iword != rword:
    print(f'Sie haben das Spiel leider verloren. Es wäre {rword} gewesen.')
