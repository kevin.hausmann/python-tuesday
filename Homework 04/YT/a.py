import random

print("Welcome to the word guessing game!")

print("good luck!!")

words = {'Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis', 'Regen',
         'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe', 'Hotel', 'Hütte',
         'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza', 'Sache', 'Sahne', 'Seife',
         'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy', 'Kleid', 'Leben', 'Radio', 'Regal',
         'Spiel', 'Steak', 'Stück'}

word: str = random.choice(tuple(words))
guess: str = ""
turns = 6
failed = 0

while failed < turns:

    for i in word:

        if i in guess:
            print(i)
        else:
            print("_")

            failed += 1

    if failed == 0:
        print("yaaa, you won")

        print(f"the word is: ", word)

        guess = input("guess a character:")

        guesses += guess

        if guess not in word:
            turns -= 1
            print("Wrong")
            print("You have", + turns, 'more guesses')

            if turns == 0:
                print("You Loose")
