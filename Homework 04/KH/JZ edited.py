import random

print('Hallo welcome to the game!')
print('''Please guess the secret word which is chosen from the list below.
    When the answer is wrong, you will get hint whether the letter exist (hint:0) or NOT (hint:1)
    You have 6 chances''')
words = {'Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis', 'Regen',
         'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe', 'Hotel', 'Hütte',
         'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza', 'Sache', 'Sahne', 'Seife',
         'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy', 'Kleid', 'Leben', 'Radio', 'Regal',
         'Spiel', 'Steak', 'Stück'}
secret_word = random.choice(tuple(words))
secret_word = secret_word.upper()

# secret_word = LEBEN, guess = APFEL
# 1. Runde: position = 0, char = 'A', index_list = [] -> hint = 0
# 2. Runde: position = 1, char = 'P', index_list = [] -> hint = 0
# 3. Runde: position = 2, char = 'F', index_list = [] -> hint = 0
# 4. Runde: position = 3, char = 'E', index_list = [1, 3] -> hint = 2
# 5. Runde: position = 4, char = 'L', index_list = [0] -> hint = 1

# 'YAMAN' , 'A' -> index_list = [1, 3]
# 'ANNA', 'A' -> index_list = [0, 3]

def char_comparison() -> str:
    """Compares guessed work to secret, returns user hint text."""
    hint = []
    for position, char in enumerate(guess):
        index_list = []
        for index, letter in enumerate(secret_word):
            if letter == char:
                index_list.append(index)

        # korrekt = 2, kommt vor = 1, kommt nicht vor = 0
        if char in secret_word:
            if position in index_list:
                hint.append(2)
            else:
                hint.append(1)
        else:
            hint.append(0)

    return str(hint)


chances = 6
attempts = 0
guess = ''
while guess != secret_word and attempts < chances:
    guess = input('please input your guessed word:').upper()

    print(f'You still have {chances - attempts} chances.Read the Hint and try again.')
    print(char_comparison())
    attempts += 1

if guess == secret_word:
    print(f'Awesome! You got the secret word {secret_word} in {attempts} attempts!')
else:
    print(f'You lose, my Wordle was {secret_word}!')
