import random

words: set[str] = {word.upper() for word in
                   {'Anruf', 'Anzug', 'Apfel', 'April', 'Beruf', 'Durst', 'Hafen', 'Kunde', 'Laden', 'Markt', 'Preis',
                    'Regen', 'Stuhl', 'Tisch', 'Vater', 'Bluse', 'Couch', 'Decke', 'Firma', 'Frage', 'Größe', 'Hilfe',
                    'Hotel', 'Hütte', 'Kasse', 'Kette', 'Küche', 'Lampe', 'Menge', 'Mütze', 'Natur', 'Pause', 'Pizza',
                    'Sache', 'Sahne', 'Seife', 'Socke', 'Sonne', 'Übung', 'Woche', 'Wurst', 'Alter', 'Datum', 'Handy',
                    'Kleid', 'Leben', 'Radio', 'Regal', 'Spiel', 'Steak', 'Stück'}}
max_attempts: int = 6
attempts: int = 0

secret: str = random.choice(tuple(words))
guess: str = ''
hint: str = 'XXXXX'
ex_chars: list[str] = []

while attempts < max_attempts and guess != secret:
    guess = input(f'Round {attempts + 1}! What is my Wordle? Hint: {hint}, exclude {ex_chars}').upper()

    if guess not in words:
        print('This is not in my dictionary. Please try again!')
    else:
        hint = ''
        for index, char in enumerate(guess):
            if char in secret:
                # Chars may occur more than once, so a simple secret.index() is not enough here!
                char_at_indices = [idx for idx, letter in enumerate(secret) if letter == char]
                hint += '✓' if index in char_at_indices else '↔'
            else:
                hint += 'x'
                ex_chars = sorted(ex_chars + [char]) if char not in ex_chars else ex_chars

        attempts += 1

print('You won!' if guess == secret else f'You lose! My Wordle was "{secret}".')
