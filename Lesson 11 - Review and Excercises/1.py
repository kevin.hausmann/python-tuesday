def sqrt(value: int) -> float:
    """
    Calculates square root of given value.

    :param value: Value to get square root from.
    :return: The square root of value, or zero if bad input is given.
    """
    return value ** (1 / 2) if isinstance(value, (int, float)) and value > 0 else 0.


def test_sqrt():
    assert 3. == sqrt(9)
    assert 0. == sqrt(0)
    assert 0. == sqrt(None)
    assert 0. == sqrt('Blabla')
    assert 0. == sqrt(-1)
    assert 3. == sqrt(9)
    assert 3. == sqrt(9.)
    assert 1. == sqrt(True)
    assert 0. == sqrt(False)


test_sqrt()

numbers = [True, 9, 25, 36, True, False, 'Blabla']
for index, number in enumerate(numbers):
    if isinstance(number, bool):
        print(f'Die Wurzel von {number} ist {sqrt(number)} (Eintrag Nummer {index + 1})!')
