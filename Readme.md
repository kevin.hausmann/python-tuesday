## Lesson 01
* What is Python?
* Variables and data types
* Homework: Implement game "Guess my number!"

## Lesson 02
* Data types (again)
* Program control flow and how to influence it
* Homework: "Guess my number!" inverse

## Lesson 03
* Data types and program control flow
* Lists and for loops
* Homework: Time series and emission calculation

## Lesson 04
* Collections: Lists, sets, tuples and dictionaries
* Even more for loops
* Homework: Implement you own version of "Wordle"

## Lesson 05
* Getting started with functions
* Homework: Represent time series as dicts
* Homework: Get yourself on code-de.org

## Lesson 06
* Read from files
* Parse csv data into dict time series
* Homework: Implement the 'read_time_series' function

## Lesson 07
* Review different implementations of the 'read_time_series' function
* Identify additional functions need to interface with the database
* Homework: Implement one or more of the additional functions

## Lesson 08
* Why do we need APIs?
* Implementations for the additional API functions
* Homework: Implement a CalQlator script, using our API,
  that calculates "#MyEmissions = (#52001331 + #52044111) * #52001122"
  and stores the results in our csv database.
  Extra: Add your own calculation script!
  
## Lesson 09
* Using the API to implement CalQlator scripts
* Even more API function implementations
* Homework: Implement the init_database_with_dummy_data() as well as the
  read_time_series_matching_filter() functions. Use the latter to implement
  the automatic emission calculation (AEB)!
  
## Lesson 10
* Implementing complex calculation scripts based on our API
* Next steps, where do we go from here?
