"""This is Homework 3

Create two time series: activity data (AD) and emission factor (EF)
Select start and stop years (e.g. 1990 to 2020)
Fill time series with random values for all years
AD values should be 100 +- random 20
EF values should be 0.5 +- random .3
Calculate (EMyear = ADyear * EFyear) and store emissions in new time series (EM)
Print yearly and total results

"""

import random

import numpy

PARADESC = {"Region": "GB", "Sector": "Agriculture", "Fuel": "Bio_Gas"}
PARAAD = {"value": "activity_data", "unit": "MWh"}
PARAEF = {"value": "emission_factor", "unit": "kg/MWh"}
PARAEM = {"value": "CO2_Emission", "Einheit": "kg"}


class Ha3_numpy:
    def __init__(self, start: int, stop: int, adfactor: int = 100, effactor: float = 0.5):
        """
        Numpy Solution
        :param start: First year of your timeseries
        :param stop: Last year of your timeseries
        :param adfactor: Activity Data number HW (100)
        :param effactor: EMission Factor number HW (0.5)
        """
        self.startdate = start
        self.stopdate = stop
        self.years = numpy.arange(self.startdate, self.stopdate + 1)
        self.ts = numpy.ones_like(self.years)
        self.ad = self.ts * adfactor + numpy.random.randint(-20, 20, size=self.years.shape[0])
        self.ef = self.ts * effactor + numpy.random.uniform(-0.3, 0.3, [self.years.shape[0]])
        self.emyr = self.ef * self.ad
        emission_ts = numpy.dstack((self.years, self.emyr))
        print(f"This is the whole timeseries: {emission_ts}")
        print(f"This is the overall total {numpy.sum(self.emyr)}")


class HA3_lists:
    def __init__(self, start: int, stop: int, adfactor: int = 100, effactor: float = 0.5):
        """
        List Solution
        :param start: First year of your timeseries
        :param stop: Last year of your timeseries
        :param adfactor: Activity Data number HW (100)
        :param effactor: EMission Factor number HW (0.5)
        """
        self.startdate = start
        self.stopdate = stop + 1
        self.years = [year for year in range(self.startdate, self.stopdate)]
        self.ef = [effactor + random.uniform(-0.3, 0.3) for j in range(len(self.years))]
        self.ad = [adfactor + random.uniform(-20, 20) for j in range(len(self.years))]
        self.summ = 0
        self.emyr = []
        for efindex, ef in enumerate(self.ef):
            self.summ += (self.ef[efindex] * self.ad[efindex])
            self.emyr.append(ef * self.ad[efindex])
        self.emlistus = list(zip(self.years, self.emyr))
        print(f"This is the whole timeseries: {self.emlistus}")
        print(f"This is the overall total {self.summ}")


class HA5_dict:
    def __init__(self, start: int, stop: int, adfactor: int = 100, effactor: float = 0.5):
        """
        Dict Solution
        :param start: First year of your timeseries
        :param stop: Last year of your timeseries
        :param adfactor: Activity Data number HW (100)
        :param effactor: EMission Factor number HW (0.5)
        """
        self.startdate = start
        self.stopdate = stop + 1
        self.years_keys = [yr for yr in range(self.startdate, self.stopdate)]
        self.ef_vals = [effactor + random.uniform(-0.3, 0.3) for j in range(len(self.years_keys))]
        self.ad_vals = [adfactor + random.uniform(-20, 20) for j in range(len(self.years_keys))]
        self.ef_dict = dict(zip(self.years_keys, self.ef_vals))
        self.ad_dict = dict(zip(self.years_keys, self.ad_vals))
        self.summ = 0
        self.emyr = {}
        self.em_total = {}
        for year in self.years_keys:
            self.summ += (self.ef_dict[year] * self.ad_dict[year])
            self.emyr.update({year: (self.ef_dict[year] * self.ad_dict[year])})
            self.em_total.update({year: self.summ})
        self.ef_dict.update(**PARADESC, **PARAEF)
        self.ad_dict.update(**PARADESC, **PARAAD)
        self.emyr.update(**PARADESC, **PARAEM)
        self.em_total.update(**PARADESC, **PARAEM)
        print(f"This is the whole timeseries: {self.emyr}")
        print(f"This is the overall total {self.em_total}")
