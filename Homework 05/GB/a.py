import random as rd

ad: list = []
ef: list = []
time: list = []
em1: list = []

ts: int = 1990
te: int = 2020

while ts <= te:
    a: int = rd.randint(80, 120)
    e: float = rd.uniform(0.2, 0.8)

    time += [ts]
    ad += [a]
    ef += [e]

    # Multiplikation der Listenelemente bevor sie in die Liste Em1 geschrieben werden
    emission1 = a * e
    em1 += [emission1]

    ts += 1

activity_data = zip(time, ad)
emission_factor = zip(time, ef)
emissions = zip(time, em1)

dict_activity_data: dict = dict(activity_data)
dict_emission_factor: dict = dict(emission_factor)
dict_emission: dict = dict(emissions)

dict_activity_data["ID"] = '#003'
dict_activity_data["Wertetyp"] = 'Aktivitätsrate'
dict_activity_data["Schadstoff"] = ''
dict_activity_data["Energieträger"] = 'Steinkohle'
dict_activity_data["Sektor"] = 'Industriewärmekraftwerke'

dict_emission_factor.update(
    {"ID": '#002', "Wertetyp": 'Emissionsfaktor', "Schadstoff": 'CO2', "Energieträger": 'Steinkohle',
     "Sektor": 'Industriewärmekraftwerke'})
dict_emission.update({"ID": '#003', "Wertetyp": 'Emissions', "Schadstoff": 'CO2', "Energieträger": 'Steinkohle',
                      "Sektor": 'Industriewärmekraftwerke'})

print(f'Dictionary Activity Data {dict_activity_data}')
print(f'Dictionary Emissionfactor {dict_emission_factor}')
print(f'Dictionary Emissions {dict_emission}')
