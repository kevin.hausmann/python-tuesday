import random as rd

activity_data: dict = {"ID": '#001', "Wertetyp": 'Aktivitätsrate', "Schadstoff": '', "Energieträger": 'Steinkohle',
                       "Sektor": 'Industriewärmekraftwerke'}
emission_factor: dict = {"ID": '#002', "Wertetyp": 'Emissionsfaktor', "Schadstoff": 'CO2',
                         "Energieträger": 'Steinkohle', "Sektor": 'Industriewärmekraftwerke'}
emissions: dict = {"ID": '#003', "Wertetyp": 'Emissions', "Schadstoff": 'CO2', "Energieträger": 'Steinkohle',
                   "Sektor": 'Industriewärmekraftwerke'}

ts: int = 1990
te: int = 2020

while ts <= te:
    a: int = rd.randint(80, 120)
    e: float = rd.uniform(0.2, 0.8)

    activity_data[ts] = a
    emission_factor[ts] = e
    emissions[ts] = a * e

    ts += 1

print(f'Dictionary Activity Data {activity_data}')
print(f'Dictionary Emissionfactor {emission_factor}')
print(f'Dictionary Emissions {emissions}')
