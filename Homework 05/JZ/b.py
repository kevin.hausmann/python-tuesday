'''Implement homework 3 using dicts,
add dimension/descriptors to the time series as well as data'''

import random

start_year = 1990
stop_year = 2020
period = range(start_year, stop_year + 1)

year_list = [i for i in period]
activity_data = [100 + random.randint(-20, 20) for i in period]
emission_factor = [0.5 + random.uniform(-.3, .3) for i in period]
emission = [ad * ef for ad, ef in zip(activity_data, emission_factor)]

para_default = {"Region":"DE", "Sektor":"Chemische Industrie","Brennstoff": "Gas"}

para_ad = {"Wertetyp": "activity_data","Einheit":"t"}
data_ad = {year_list[i]:activity_data[i] for i in range(len(period))}


para_ef = {"Wertetyp": "emission_factor", "Einheit":"kg/t"}
data_ef = {year_list[i]:emission_factor[i] for i in range(len(period))}

para_em = {"Wertetyp": "CO2_Emission", "Einheit":"kg"}
data_em = {year_list[i]:emission[i] for i in range(len(period))}

dict_ad = {**para_ad,**para_default,**data_ad}
dict_ef = {**para_ef,**para_default,**data_ef}
dict_em = {**para_em,**para_default,**data_ad}

print(dict_ad)
print(dict_ef)
print(dict_em)
print(sum(emission))
