import random

start_year = 1990
stop_year = 2020
period = range(start_year, stop_year + 1)

year_list = [i for i in period]
activity_data = [100 + random.randint(-20, 20) for i in period]
emission_factor = [0.5 + random.uniform(-.3, .3) for i in period]

dict_ad = {year_list[i]:activity_data[i] for i in range(len(period))}
dict_ef = {year_list[i]:emission_factor[i] for i in range(len(period))}

print(dict_ad)
print(dict_ef)
