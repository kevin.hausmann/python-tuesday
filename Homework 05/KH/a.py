import random

start_year = 1990
stop_year = 2020

activity_data = {'Wertetyp': 'AR',
                 'Region': 'France',
                 'Sektor': 'Eisenbahn',
                 'Brennstoff': 'Diesel'}
emission_factor = {'Wertetyp': 'EF',
                   'Region': 'France',
                   'Sektor': 'Eisenbahn',
                   'Brennstoff': 'Diesel',
                   'Schadstoff': 'CO2'}
emission = {'Wertetyp': 'EM',
            'Region': 'France',
            'Sektor': 'Eisenbahn',
            'Brennstoff': 'Diesel',
            'Schadstoff': 'CO2'}

for year in range(start_year, stop_year + 1):
    activity_data[year] = 100 + random.randint(-20, 20)
    emission_factor[year] = 0.5 + random.uniform(-.3, .3)
    emission[year] = activity_data[year] * emission_factor[year]

print(activity_data)
print(emission_factor)
print(emission)

total = 0
for year in range(start_year, stop_year + 1):
    total = total + emission[year]
print(total)
