import random

start_year = 1990
stop_year = 2020
period = range(start_year, stop_year + 1)

default_values = {'Region': 'France',
                  'Sektor': 'Eisenbahn',
                  'Brennstoff': 'Diesel'}

activity_data = {'Wertetyp': 'AR'} | default_values | {year: 100 + random.randint(-20, 20) for year in period}
emission_factor = {'Wertetyp': 'EF'} | default_values | \
                  {'Schadstoff': 'CO2'} | {year: 0.5 + random.uniform(-.3, .3) for year in period}
emission = {'Wertetyp': 'EM'} | default_values | \
           {'Schadstoff': 'CO2'} | {year: activity_data[year] * emission_factor[year] for year in period}

print(activity_data)
print(emission_factor)
print(emission)
print(sum(emission[year] for year in period))
