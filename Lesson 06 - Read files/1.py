#(0. Check if function already exists, do NOT re-invent the wheel)
# 1. Define function header
# 2. Add documentation of intended use
# 3. Put test cases
# 4. Implement

DATABASE_FILENAME = 'database.csv'


def read_time_series(identifier: str) -> dict | None:
    """
    Read and return time series with given ID from database.

    This method searches the database for any time series with the given identifier. If found,
    the corresponding series is returned, including all its data. If not found, None is returned.

    :param str identifier: Time series ID as stored in the database.

    :return: Series as a dict or None if ID does not exist. Time series year keys in dict are ints,
    values will be floats. All other info (unit, name etc) is represented as strings. Empty values are omitted.
    """

    # Open database file
        # Use python csv library to access values in file (use correct formatting options!)
        # Go trough all lines in file
            # Check if we found the correct series (i.e. the id matches)
                # Create empty dict
                # Check all values in current line and exclude empties ("None")
                    # We want the 'year' keys to be ints not strs, so we need to convert those
                    # Finally, put value
    # Return dict or None if identifier not found

    return None


def test_read_time_series():
    assert None == read_time_series(None)
    assert None == read_time_series('XXXX--key that cannot possibly exist!--XXXX')

    assert {'ID': 'empty'} == read_time_series('empty')
    assert {'ID': 'dummy', 'Name': 'Dummy'} == read_time_series('dummy')

    example_series = read_time_series('example')
    assert 'example' == example_series['ID']
    assert 'ExFuel' == example_series['Brennstoff']
    assert 'EX' == example_series['Einheit']
    assert 2000.5 == example_series[2000]


test_read_time_series()
