# Implement "Galgenraten"
import random

MAX_TRIES = 10

# Read a random word from words.txt
with open('words.txt', 'r', encoding='UTF8') as file:
    words = file.readlines()

secret = random.choice(words).rstrip()

print(f'Hallo, Willkommen bei Galgenraten! Mein Wort hat {len(secret)} Buchstaben - kannst du es raten?')

# Allow user to guess a letter
guessed_letters = set()
user_input = ''
while len(guessed_letters) < MAX_TRIES:
    user_input = input('Bitte gib einen Buchstaben oder das Lösungswort ein: ')

    if user_input == secret:
        break
    elif len(user_input) == 1 and user_input.isalpha() and user_input not in guessed_letters:
        guessed_letters.add(user_input.lower())
    else:
        print('Das war kein gültiger oder ein schon geratener Buchstabe!')

    # Check if letter exists in selected word (write a full function for this, including doc and tests!)
    feedback = ''.join(letter if letter.lower() in guessed_letters else '_' for letter in secret)
    # Count the number of letters guessed before the user solves the riddle, give proper feedback to user
    print(f'Runde {len(guessed_letters)}/{MAX_TRIES}, Stand: {feedback}, bereits geraten: {sorted(guessed_letters)}')

print('Wow! Das stimmt, gut geraten!' if user_input == secret else 'Schade!')
