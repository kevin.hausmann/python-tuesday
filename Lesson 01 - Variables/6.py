greeting: str = "Hello!" # Zeichenkette
result: int = 42 # Ganzzahl, int, long
pi: float = 3.14 # Kommazahl, float, double, decimal
can_program: bool = True # Wahrheitswert, boolean TRUE/FALSE, Python: True / False

def show_type(variable):
    test = "Bla"
    print(test)
    print(type(variable))
    print("Quark")

show_type(pi)
show_type(can_program)
print(test)
